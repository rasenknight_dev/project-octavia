import discord
import youtube_dl
from discord.ext import commands
import asyncio
from itertools import cycle

TOKEN = 'NTA3OTc1MjQ1MzUzODQ0NzU2.Ds9mxQ.kgUzaTLEERaVdNIxGfUmndkd7rQ'

client = commands.Bot(command_prefix='*')
client.remove_command('help')

status = ['Liset Playlist','Ordis Playlist','Lua Puzzle Music']
players = {}
queues = {}

def check_queue(id):
    if queues[id]!= []:
        player = queues[id].pop(0)
        players[id] = player
        player.start()

@client.event
async def on_ready():
    await client.change_presence(game=discord.Game(name='Ordis Playlist'))
    print('Octavia is in the house')

@client.event
async def on_member_join(member):
    server =  member.server
    role = discord.utils.get(server.roles, name= 'Newbie')
    await client.add_roles(member, role)
@client.command()
async def ping():
    await client.say('Pong!')

@client.command()
async def echo(*args):
    output = ''
    for word in args:
        output += word
        output += ' '
    await client.say(output)

@client.command(pass_context=True)
async def clear(ctx, amount=3):
    channel = ctx.message.channel
    messages = []
    async for message in client.logs_from(channel,limit=int(amount)+1):
        messages.append(message)
    await client.delete_messages(messages)
    await client.say('Messages deleted')

async def change_status():
    await client.wait_until_ready()
    msgs = cycle(status)

    while not client.is_closed:
        current_status = next(msgs)
        await client.change_presence(game=discord.Game(name=current_status))
        await asyncio.sleep(3600)

@client.command(pass_context=True)
async def help(ctx):
    author = ctx.message.author
    embed = discord.Embed(
        title = 'Help Commands',
        description = 'Commands to interact with Octavia',
        colour = discord.Colour.orange()
    )
    embed.set_author(name='Octavia', icon_url='https://media.sketchfab.com/urls/ae5a8bb369264e25aef6914958aab545/dist/thumbnails/a56e1d6856044f68a12b73cd2ba64ef1/488c1f06c8044f68af815ca4dc09c171.jpeg')
    embed.add_field(name='*help', value='Send Help Commands to the user that ask for it.', inline=False)
    embed.add_field(name='*ping', value='Return Pong!.', inline=False)
    embed.add_field(name='*echo [...]', value='Return whatever the user write in this command.', inline=False)
    embed.add_field(name='*clear [...]', value='Remove the number of text lines you had send as a parameter.', inline=False)
    await client.send_message(author,embed=embed)

@client.command(pass_context=True)
async def join(ctx):
    channel = ctx.message.author.voice.voice_channel
    await client.join_voice_channel(channel)
    await client.say('{} call me to join to the party!'.format(ctx.message.author))

@client.command(pass_context=True)
async def leave(ctx):
    server = ctx.message.server
    voice_client = client.voice_client_in(server)
    await voice_client.disconnect()
    await client.say('See you soon!')

@client.command(pass_context=True)
async def play(ctx, url):
    channel = ctx.message.author.voice.voice_channel
    await client.join_voice_channel(channel)
    await client.say('Playing {}'.format(url))
    server = ctx.message.server
    voice_client = client.voice_client_in(server)
    player = await voice_client.create_ytdl_player(url, after=lambda: check_queue(server.id))
    players[server.id] = player
    player.start()

@client.command(pass_context=True)
async def pause(ctx):
    id = ctx.message.server.id
    players[id].pause()

@client.command(pass_context=True)
async def stop(ctx):
    id = ctx.message.server.id
    players[id].stop()

@client.command(pass_context=True)
async def resume(ctx):
    id = ctx.message.server.id
    players[id].resume()

@client.command(pass_context=True)
async def queue(ctx,url):
    server = ctx.message.server
    voice_client = client.voice_client_in(server)
    player = await voice_client.create_ytdl_player(url, after=lambda: check_queue(server.id))

    if server.id in queues:
        queues[server.id].append(player)
    else:
        queues[server.id] = [player]
    await client.say('Song added to the queue.')

client.loop.create_task(change_status())
client.run(TOKEN,host='0.0.0.0', port=6000,debug=True)

# @client.event
# async def on_message(message):
#     author = message.author
#     content = message.content
#     print ('{}:{}'.format(author,content))

# @client.event
# async def on_message_delete(message):
#     author = message.author
#     content = message.content
#     channel = message.channel
#     await client.send_message(channel,'{}:{}'.format(author,content))

# @client.command()
# async def displayembed():
#     embed = discord.Embed(
#         title = 'Title',
#         description = 'This is the description',
#         colour = discord.Colour.magenta()
#     )
#     embed.set_footer(text='This is a footer.')
#     embed.set_image(url='https://media.sketchfab.com/urls/ae5a8bb369264e25aef6914958aab545/dist/thumbnails/a56e1d6856044f68a12b73cd2ba64ef1/488c1f06c8044f68af815ca4dc09c171.jpeg')
#     embed.set_thumbnail(url='https://media.sketchfab.com/urls/ae5a8bb369264e25aef6914958aab545/dist/thumbnails/a56e1d6856044f68a12b73cd2ba64ef1/488c1f06c8044f68af815ca4dc09c171.jpeg')
#     embed.set_author(name='Author Name', icon_url='https://media.sketchfab.com/urls/ae5a8bb369264e25aef6914958aab545/dist/thumbnails/a56e1d6856044f68a12b73cd2ba64ef1/488c1f06c8044f68af815ca4dc09c171.jpeg')
#     embed.add_field(name='Field Name', value='Field Value', inline=False)
#     embed.add_field(name='Field Name', value='Field Value', inline=True)
#     embed.add_field(name='Field Name', value='Field Value', inline=True)
#     await client.say(embed=embed)

# @client.event
# async def on_reaction_add(reaction, user):
#     channel = reaction.message.channel
#     await client.send_message(channel,'{} has added {} to the message: {}'.format(user.name,reaction.emoji, reaction.message.content))
#
# @client.event
# async def on_reaction_remove(reaction,user):
#     channel = reaction.message.channel
#     await client.send_message(channel,'{} has remove {} from the message: {}'.format(user.name,reaction.emoji, reaction.message.content))